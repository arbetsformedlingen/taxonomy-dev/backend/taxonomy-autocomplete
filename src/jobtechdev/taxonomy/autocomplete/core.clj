(ns jobtechdev.taxonomy.autocomplete.core
  (:gen-class)
  (:require
   [jobtechdev.taxonomy.autocomplete.database :as database]
   [taoensso.nippy :as nippy]
   [clojure.string :as st]
   [mount.core :refer [start]]
   [clojure.java.io :as io]
   [clj-commons.byte-streams :as bs]
   [cpath-clj.core :as cp])
  (:import
   [org.apache.lucene.analysis Analyzer]
   [org.apache.lucene.analysis.custom CustomAnalyzer]
   [org.apache.lucene.analysis.custom CustomAnalyzer]
   [org.apache.lucene.store MMapDirectory Directory]
   [java.nio.file.attribute FileAttribute]
   [org.apache.lucene.search.suggest.analyzing AnalyzingInfixSuggester]
   [org.apache.lucene.search.suggest InputIterator Lookup Lookup$LookupResult]
   [org.apache.lucene.util BytesRef]
   [java.util Arrays]
   [clojure.lang RT]
   [java.nio.file Paths Path Files LinkOption]
   [java.net URI]))

(comment
  "TODO use as a library in mentor api"
  "TODO export all versions as a build step"
  "TODO Split namespace in creating index and using index")


(defn build-analyzer []
  (.build (doto (CustomAnalyzer/builder)
            (.withTokenizer "standard" (into-array String []))
            (.addTokenFilter "lowercase" (into-array String []))
            (.addTokenFilter "asciiFolding" (into-array String ["preserveOriginal" "true"])))))

(def ^Analyzer analyzer (build-analyzer))

;;;;;;;;;;;;;;;;;;;;;;

(def lucene-index-directory-name "lucene-index/")

(def lucene-index-resource-directory-name (str "resources/" lucene-index-directory-name))

(def writable-directory "/tmp/")

(def lucene-path-in-tmp (Path/of
                         (str writable-directory lucene-index-directory-name)
                         (into-array  [""])))

(defn create-directory! []
  (Files/createDirectory lucene-path-in-tmp (into-array FileAttribute [])))

(defn copy-files-from-jar! [url]
  (doseq [[path uris] (cp/resources url)
          :let [uri (first uris)
               ;; relative-path (subs path 1)
               ;; _ (println relative-path)
                output-file (io/file (str writable-directory lucene-index-directory-name)
                                     path)]]
    (with-open [in (io/input-stream uri)]
      (io/copy in output-file))))

(defn can-write-to-lucene-dir-parent []
  (Files/isWritable (Path/of writable-directory (into-array [""]))))

(defn lucene-directory-exists-in-tmp? []
  (Files/exists lucene-path-in-tmp (into-array LinkOption [])))

(defn extract-lucene-index-from-jar! [url]
  (if (lucene-directory-exists-in-tmp?)
    lucene-path-in-tmp
    (if (can-write-to-lucene-dir-parent)
      (do
        (let [directory (create-directory!)]
          (copy-files-from-jar! url)
          directory))
      (throw (Exception. (str "Can't extract jar file. Can't create a directory in " writable-directory))))
    ))


(defn ^Path get-lucene-index-directory []
  (let [lucene-index-url (io/resource lucene-index-directory-name)]
    (case (.getProtocol lucene-index-url)
      "file" (Path/of (.toURI lucene-index-url))
      "jar" (extract-lucene-index-from-jar! lucene-index-url)
      (throw (Exception. (str "Unknown protocol for lucene index URL. " lucene-index-url))))))

;; (.toURI (io/resource lucene-index-directory-name))
;; Create directory or return existing one

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defn- default-to-1 [_] 1)

(defn-  InputIterator-on
  [^InputIterator seqable & {:keys [text-fn weight-fn payload-fn contexts-fn]
              :or {text-fn str
                   weight-fn default-to-1
                   payload-fn nil
                   contexts-fn nil}}]
  (let [iter (RT/iter seqable)
        current-vol (volatile! nil)]
    (reify InputIterator
      (next [_]
        (when (.hasNext iter)
          (let [current (.next iter)]
            (vreset! current-vol current)
            (BytesRef. (str (text-fn current))))))
      (weight [_]
        (weight-fn @current-vol)) ;; more alt labels = better
      (hasPayloads [_]
        (some? payload-fn))
      (payload [_]
        (BytesRef. ^bytes (nippy/freeze (payload-fn @current-vol))))
      (hasContexts [_]
        (some? contexts-fn))
      (contexts [_]
        (->> @current-vol contexts-fn (map #(BytesRef. (str %))) set)))))

(defn create-infix-suggester []
   (AnalyzingInfixSuggester.
      (MMapDirectory. (get-lucene-index-directory))
      #_index-analyzer analyzer
      #_query-analyzer analyzer
      #_min-prefix-chars 4
      #_commit-on-build true
      #_all-terms-required false
      #_highlight true
      #_closeIndexWriterOnBuild true))

(def swedish-regions #{"DQZd_uYs_oKb"  "oDpK_oZ2_WYt"
                       "zupA_8Nt_xcD"  "wjee_qH2_yb6" "65Ms_7r1_RTG"
                       "MtbE_xWT_eMi"  "9QUH_2bb_6Np" "tF3y_MF9_h5G"
                       "9hXe_F4g_eTG" "xTCk_nT5_Zjm"
                       "oLT3_Q9p_3nn" "CaRE_1nn_cSU"
                       "s93u_BEb_sx2" "CifL_Rzy_Mku"
                       "zBon_eET_fFU" "K8iD_VQv_2BA"
                       "EVVp_h6U_GSZ" "g5Tt_CAV_zBd"
                       "G6DV_fKE_Viz" "NvUF_SP1_1zo" "zdoY_6u5_Krt"})

(defn swedish-region? [id]
  (contains? swedish-regions id))

(defn region-but-not-swedish? [concept]
  (and
   (= "region" (:concept/type concept))
   (not (swedish-region? (:concept/id concept)))))

(defn prioritize-alt-labels-weight-fn [concepts-list]
  (let [c (first concepts-list)
        w (+ (count (:concept/alternative-labels c)) (count (:concept/hidden-labels c)))]
    (if (region-but-not-swedish? c)
      -1
      (if (= w 0)
        1
        w))))

(defn load-lookup  []
  (create-infix-suggester))

(defn extract-strings-from-concept [{:concept/keys
                                     [preferred-label alternative-labels hidden-labels]}]
  (flatten [preferred-label alternative-labels hidden-labels]))

(defn concept-list-to-search-string [concepts-list]
  (st/join " " (remove nil? (mapcat extract-strings-from-concept (flatten concepts-list)))))

(defn concept-list-context-fn [concepts-list]
  (let [c (first concepts-list)]
    [(:concept/type c) (:concept/id c)]))

(defn create-lookup [all-concepts]
  (doto
   (create-infix-suggester)
    (.build (InputIterator-on
             all-concepts ;; (fetch-all-concepts-including-related db)
             :payload-fn first
             :contexts-fn concept-list-context-fn
             :text-fn concept-list-to-search-string
             :weight-fn prioritize-alt-labels-weight-fn))))

(defn autocomplete [{:keys [;;required
                                      query-string
                                      ;; version
                                      ;; optional
                                      type ;; coll of strings, ["occupation-name" "skill"]
                                      ;; offset
                                      limit]}]
  (let [^Lookup lookup (load-lookup)
        result (.lookup lookup
                        query-string
                        (set (map #(BytesRef. (str %)) type))
                        true
                        (or limit 10))
        result2 (map #(let [bytes-ref (.-payload ^Lookup$LookupResult %)]
                        (nippy/thaw (Arrays/copyOfRange (.-bytes bytes-ref)
                                                        (.-offset bytes-ref)
                                                        (+ (.-offset bytes-ref)
                                                           (.-length bytes-ref))))) result)]

    result2))

(defn build-lucene-index-of-latest-taxonomy-version!
  "Please note that when you create the lucene index directory you need to
  point out the resources folder. But when you load it you should not include the
  resource folder name in the path."
  []
  (start)
  (Files/createDirectory (Path/of
                          lucene-index-resource-directory-name
                          (into-array  [""]))
                          (into-array FileAttribute []))
  (create-lookup (database/fetch-all-concepts-latest-version)))

(comment
  (autocomplete
   {:query-string "Dagis"
    })

  (autocomplete
   {:query-string "s"
    :type ["region" #_"occupation-field"]
    }))
