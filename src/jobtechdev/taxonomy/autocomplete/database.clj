(ns jobtechdev.taxonomy.autocomplete.database
  (:require
   [datahike.api :as d]
   [taoensso.nippy :as nippy]
   [clojure.string :as st]
   [wanderung.core :as w]
   [mount.core :refer [defstate]]
   ))

(def datahike-cfg
  {:wanderung/type :datahike
   :store         {:backend :mem
                   :id      "taxonomy"}
   :index :datahike.index/persistent-set
   :keep-history? true})

(def nippy-cfg
  {:wanderung/type :nippy
   :filename "resources/taxonomy-database.nippy"})

(defn load-database []
  (w/migrate nippy-cfg datahike-cfg))


(defn create-conn []
  (do
    (load-database)
    (d/connect datahike-cfg)))

(defstate  ^{:on-reload :noop}
  conn :start (create-conn))


;; DATALOG QUERIES

(defn fetch-all-concepts-with-deprecated-references [db]
  (d/q
   '[:find (pull ?rep [:concept/id
                       :concept/type
                       :concept/preferred-label
                       :concept/alternative-labels
                       :concept/hidden-labels
                       :concept/replaced-by
                       :concept/deprecated])
     (pull ?c [:concept/id
               :concept/type
               :concept/preferred-label
               :concept/alternative-labels
               :concept/hidden-labels
               :concept/replaced-by
               :concept/deprecated])

     :where
     [?c :concept/deprecated true]
     [?c :concept/replaced-by ?rep]]
   db))

(defn fetch-all-concepts-with-keywords [db]
  (d/q
   '[:find (pull ?c [:concept/id
                     :concept/type
                     :concept/preferred-label
                     :concept/alternative-labels
                     :concept/hidden-labels])
     (pull ?ck [:concept/id
                :concept/type
                :concept/preferred-label
                :concept/alternative-labels
                :concept/hidden-labels])
     :where
     ;;     [?ck :concept/preferred-label "Daghemsföreståndare"]
     [?ck :concept/type "keyword"]
     [?r :relation/concept-2 ?ck]
     [?r :relation/concept-1 ?c]]
   db))

;; TODO use pre build lucene index

(defn fetch-concepts-simple [db]
  (d/q '[:find (pull ?c [:concept/id
                         :concept/type
                         :concept/preferred-label
                         :concept/alternative-labels
                         :concept/hidden-labels])
         :where
         [?c :concept/id]
         (not [?c :concept/deprecated true])]
       db))


;; Fetch concepts from database

(defn concept-ids-with-keyword []
  (set (map #(:concept/id (ffirst %))  (fetch-all-concepts-with-keywords))))

(defn add-related-concepts-reduce-fn [acc element]
  (update acc (first element) conj (second element)))

(defn add-related-concepts [simple-concept-map related-concepts]
  (reduce add-related-concepts-reduce-fn simple-concept-map related-concepts))

(defn build-simple-concept-map-reduce-fn [acc element]
  (assoc acc (first element) []))

(defn build-simple-concept-map [db]
  (reduce build-simple-concept-map-reduce-fn  {}
          (fetch-concepts-simple db)))

(defn get-keyword-preferred-label [e] (:concept/preferred-label (second e)))

(defn fetch-all-concepts-including-related [db]
    (-> (build-simple-concept-map db)
      (add-related-concepts (fetch-all-concepts-with-keywords db))
      (add-related-concepts (fetch-all-concepts-with-deprecated-references db))))


(def get-database-time-point-by-version-query
  '[:find ?tx
    :in $ ?version
    :where
    [?t :taxonomy-version/id ?version]
    [?t :taxonomy-version/tx ?tx]])

(defn get-transaction-id-from-version [db version]
  (ffirst (d/q get-database-time-point-by-version-query db version)))

(def get-latest-released-version-query
  '[:find (max ?version)
    :in $
    :where [_ :taxonomy-version/id ?version]])

(defn get-latest-released-version [db]
  (ffirst (d/q get-latest-released-version-query db)))

(defn fetch-all-concepts-latest-version []
  (fetch-all-concepts-including-related @conn))


;; TODO Create and index for every version of the taxonomy
