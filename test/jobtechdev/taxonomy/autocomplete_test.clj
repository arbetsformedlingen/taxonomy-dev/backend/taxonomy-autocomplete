(ns jobtechdev.taxonomy.autocomplete-test
  (:require [clojure.test :refer :all]
            [jobtechdev.taxonomy.autocomplete.core :refer :all]))


(def concepts-keyword-test-data-1
  [#:concept{:id "jCsR_WQX_CKD",
             :type "skill",
             :preferred-label "Java, programmeringsspråk",
             :alternative-labels
             ["Java"
              "Java-programmering"
              "Java-utveckling"
              "Javakompetens"
              "Javakunskap"
              "Javaprogrammering"
              "Javautveckling"]}
   []])

(deftest test-concept-to-string
  (testing "Test how to parse concept to search string"
    (is (= "Java, programmeringsspråk Java Java-programmering Java-utveckling Javakompetens Javakunskap Javaprogrammering Javautveckling"
           (concept-list-to-search-string concepts-keyword-test-data-1)))))
