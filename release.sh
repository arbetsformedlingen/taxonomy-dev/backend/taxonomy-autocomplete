#!/usr/bin/env bash
set -euo pipefail

VERSION="v1.$(git rev-list --count HEAD)"
if [ $(git rev-parse --symbolic-full-name --abbrev-ref HEAD) != "main" ]; then
    echo "not on main branch"
    exit 1
fi

if ! git rev-parse -q --verify "refs/tags/$VERSION" >/dev/null; then
    git tag $VERSION
    git push origin $VERSION
fi

echo "se.jobtechdev.taxonomy/autocomplete {:git/url \"https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/taxonomy-autocomplete.git\"
                               :git/tag \"$VERSION\"
                               :git/sha \"$(git rev-parse -q --verify "refs/tags/$VERSION" --short=7)\"}"
